
import {
    ADD_MATCH_ACTION, RECEIVE_FETCH_MATCHES, REQUEST_FETCH_MATCHES, DELETE_MATCH,
    DELETE_MATCH_SUCCEEDED, UPDATE_MATCH, UPDATE_SUCCEEDED, SEARCH_MATCH
} from './types';


export const addMatchAction = (data) => ({ type: ADD_MATCH_ACTION, data });

export const requestFetchMatches = () => ({ type: REQUEST_FETCH_MATCHES });
export const reciveFetchMatches = (data) => ({ type: RECEIVE_FETCH_MATCHES, data });

export const deleteMatchAction = (matchId) => ({ type: DELETE_MATCH, matchId });
export const deleteMatchSuccessAction = (matchId) => ({ type: DELETE_MATCH_SUCCEEDED, matchId });

export const updateMatchAction = (updatedMatch) => ({ type: UPDATE_MATCH, updatedMatch });
export const updateMatchSuccessAction = (updatedMatch) => ({ type: UPDATE_SUCCEEDED, updatedMatch });

export const searchMatchAction = (round) => ({ type: SEARCH_MATCH, round });
