import { RECEIVE_FETCH_TEAMS,REQUEST_FETCH_TEAMS } from "./types";


export const requestFetchTeams = () => ({ type: REQUEST_FETCH_TEAMS});
export const reciveFetchTeams = (data) => ({ type: RECEIVE_FETCH_TEAMS, data});


    