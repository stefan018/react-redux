import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/rootReduceer';
import createSagaMiddleware from 'redux-saga';
import { rootSaga } from './sagas/saga';
import Main from './components/Main';


const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

class App extends Component {

  render() {

    return (

      <Provider store={store}>
        <Main />
      </Provider >
    );
  }

}

export default App;

