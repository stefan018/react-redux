import { SEARCH_MATCH_SUCCEEDED } from "../actions/types";


const initialState = {
    round: ''
}

const searchMatchReducer = (data = initialState, action) => {

    switch (action.type) {
        case SEARCH_MATCH_SUCCEEDED:
            return { round: action.round };

        default:
            return data;
    }
}

export default searchMatchReducer;