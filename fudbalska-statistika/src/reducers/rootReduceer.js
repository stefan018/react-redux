import { combineReducers } from 'redux';
import teamsReducer from './teamReducer';
import matchReducer from './matchReducer';
import searchMatchReducer from './searchMatchReducer';

const allReducers=combineReducers({
    teamsReducer,
    matchReducer,
    searchMatchReducer
});

export default allReducers;