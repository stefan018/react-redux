
import {
    ADD_MATCH_ACTION, RECEIVE_FETCH_MATCHES, DELETE_MATCH_SUCCEEDED,UPDATE_SUCCEEDED,
} from '../actions/types';

const matchReducers = (data = [], action) => {
    switch (action.type) {

        case ADD_MATCH_ACTION:
            if (data.length > 0)
                action.data.id = parseInt(data[data.length - 1].id) + 1;
            else
                action.data.id = 1;
            return [...data, action.data];

        case RECEIVE_FETCH_MATCHES:
            return action.data;

        case DELETE_MATCH_SUCCEEDED:
            const filteredMatches = data.filter(match => {
                return match.id.toString() !== action.matchId.toString();
            });
            return filteredMatches;

        case UPDATE_SUCCEEDED:
            return data.map(match => {
                if (match.id.toString() === action.updatedMatch.id.toString()) {
                    return {
                        ...match,
                        round: action.updatedMatch.round,
                        Hgoals: action.updatedMatch.Hgoals,
                        HpossessionBall: action.updatedMatch.HpossessionBall,
                        Hshots: action.updatedMatch.Hshots,
                        Hfreekicks: action.updatedMatch.Hfreekicks,
                        Hcorners: action.updatedMatch.Hcorners,
                        Hoffsides: action.updatedMatch.Hoffsides,
                        Agoals: action.updatedMatch.Agoals,
                        ApossessionBall: action.updatedMatch.ApossessionBall,
                        Ashots: action.updatedMatch.Ashots,
                        Afreekicks: action.updatedMatch.Afreekicks,
                        Acorners: action.updatedMatch.Acorners,
                        Aoffsides: action.updatedMatch.Aoffsides,
                    }
                }
                else return match
            });
        default:
            return data;
    }
}

export default matchReducers;