
const urlGetMatches = "http://localhost:3000/matches";
const urlPostMatch = "http://localhost:3000/matches";
const urlDeleteMatch = "http://localhost:3000/matches";
const urlUpdateMatch = "http://localhost:3000/matches";

export const fetchMatchesService = async () => {

    const response = await fetch(urlGetMatches);
    const data = await response.json();
    return data;
}

export const addMatchService = async (newMatch) => {
    const response = await fetch(urlPostMatch, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            id: newMatch.id,
            round: newMatch.round,
            Hgoals: newMatch.Hgoals,
            HpossessionBall: newMatch.HpossessionBall,
            Hshots: newMatch.Hshots,
            Hfreekicks: newMatch.Hfreekicks,
            Hcorners: newMatch.Hcorners,
            Hoffsides: newMatch.Hoffsides,
            Hteam: newMatch.Hteam,
            Agoals: newMatch.Agoals,
            ApossessionBall: newMatch.ApossessionBall,
            Ashots: newMatch.Ashots,
            Afreekicks: newMatch.Afreekicks,
            Acorners: newMatch.Acorners,
            Aoffsides: newMatch.Aoffsides,
            Ateam: newMatch.Ateam
        }),
    });
    return (response.status === 201);
}

export const deleteMatchService = async (matchId) => {
    const urlLink = `${urlDeleteMatch}/${matchId}`;
    const response = await fetch(urlLink, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({

        }),
    });
    return (response.status === 200);
}

export const updateMatchService = async (updatedMatch) => {
    const urlLink = `${urlUpdateMatch}/${updatedMatch.id.toString()}`;
    const response = await fetch(urlLink, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            round: updatedMatch.round,
            Hgoals: updatedMatch.Hgoals,
            HpossessionBall: updatedMatch.HpossessionBall,
            Hshots: updatedMatch.Hshots,
            Hfreekicks: updatedMatch.Hfreekicks,
            Hcorners: updatedMatch.Hcorners,
            Hoffsides: updatedMatch.Hoffsides,
            Hteam: updatedMatch.Hteam,
            Agoals: updatedMatch.Agoals,
            ApossessionBall: updatedMatch.ApossessionBall,
            Ashots: updatedMatch.Ashots,
            Afreekicks: updatedMatch.Afreekicks,
            Acorners: updatedMatch.Acorners,
            Aoffsides: updatedMatch.Aoffsides,
            Ateam: updatedMatch.Ateam,
        }),
    });
    return (response.status === 200);
}
