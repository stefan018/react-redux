

const url="http://localhost:3000/teams";

export const fetchTeamsService = async() => {
    try{
        const response= await fetch(url);
        const data= await response.json();
        return data;
    }
    catch(e)
    {
        console.log(e);
    }

}