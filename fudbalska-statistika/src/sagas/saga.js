import {  put, call, takeLatest } from 'redux-saga/effects';

import { fetchTeamsService } from '../services/team-service';
import {
    REQUEST_FETCH_TEAMS, RECEIVE_FETCH_TEAMS, REQUEST_FETCH_MATCHES, RECEIVE_FETCH_MATCHES,
    ADD_MATCH_ACTION, DELETE_MATCH, DELETE_MATCH_SUCCEEDED, UPDATE_MATCH, UPDATE_SUCCEEDED, SEARCH_MATCH, SEARCH_MATCH_SUCCEEDED
} from '../actions/types';
import { addMatchService, fetchMatchesService, deleteMatchService,  updateMatchService } from '../services/match-service';


export function* rootSaga() {
    yield takeLatest(REQUEST_FETCH_TEAMS, fetchTeamsAsync);
    yield takeLatest(REQUEST_FETCH_MATCHES, fetchMathesAsync);
    yield takeLatest(ADD_MATCH_ACTION, addNewMatch);
    yield takeLatest(DELETE_MATCH, deleteMatch);
    yield takeLatest(UPDATE_MATCH, updatedMatch);
    yield takeLatest(SEARCH_MATCH, searchMatch);
}

function* fetchTeamsAsync() {
    try {
        const data = yield call(fetchTeamsService);
        yield put({ type: RECEIVE_FETCH_TEAMS, data: data });
    }
    catch (err) {
        console.log(err);
    }
}

function* fetchMathesAsync() {
    try {
        const data = yield call(fetchMatchesService);
        yield put({ type: RECEIVE_FETCH_MATCHES, data: data });
    }
    catch (err) {
        console.log(err);
    }
}

function* addNewMatch(action) {
    try {
        const result = yield call(addMatchService(action.data));
        if (result === true) {
            yield put({ type: ADD_MATCH_ACTION, data: action.data });
        }
    }
    catch (err) {
        console.log(err);
    }
}

function* deleteMatch(action) {
    try {
        const result = yield deleteMatchService(action.matchId);
        if (result === true) {
            yield put({ type: DELETE_MATCH_SUCCEEDED, matchId: action.matchId });
        }
    }
    catch (err) {
        console.log(err);
    }
}

function* updatedMatch(action)
{
    try{
        const result= yield updateMatchService(action.updatedMatch);
        if(result === true)
        {
            yield put({type: UPDATE_SUCCEEDED,updatedMatch: action.updatedMatch});
        }
    }
    catch(err)
    {
        console.log(err);
    }
}

function* searchMatch(action)
{
    try{
        yield put({type: SEARCH_MATCH_SUCCEEDED, round: action.round});
    }
    catch(err)
    {
        console.log(err);
    }

}