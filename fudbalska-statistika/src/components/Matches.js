
import React, { Component } from "react";
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { deleteMatchAction } from '../actions/match-action';


class Matches extends Component {
    state = {
        round: ''
    }

    handleRoundChange = ({ target }) => {
        let { name, value } = target;
        this.setState({ [name]: value });
        this.props.onRoundChange(value);
    }

    render() {

        let list = null;
        if (this.props.data !== null) {
            if (this.props.data instanceof Array && this.props.data.length > 0)
                list = this.props.data.map(match =>
                    <div className="match">
                        <h5 > {match.Hteam} - {match.Ateam} ({match.Hgoals} : {match.Agoals})
                            <button id={match.id}
                                onClick={
                                    (e) => {
                                        this.props.onDeleteMatchAction(e.target.id);
                                    }
                                }
                                className="btn btn-danger ">&times;
                            </button>
                            <Link to={{
                                pathname: `/utakmice/${match.id}`,
                                aboutProps: {
                                    name: match.id,
                                }
                            }}>
                                <button type="button" class="btn btn-primary">
                                    Izmeni
                                </button>
                            </Link >
                        </h5>
                    </div>
                );

            return (

                <div className="container">
                    <div className="row">
                        <h5>Unesite broj kola:</h5>
                        <input className="form-control inpt" type="text" value={this.state.round} name="round" placeholder="Broj kola:" onChange={this.handleRoundChange} />
                    </div>
                    <p>Utakmice</p>
                    {list}
                </div>
            );
        }
    }
}

const MapDispatchToProps = (dispatch) => {
    return {
        onDeleteMatchAction: (matchId) => {
            dispatch(deleteMatchAction(matchId));
        }
    }
}

export default connect(null, MapDispatchToProps)(Matches);