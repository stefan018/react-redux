import React, { Component } from 'react';
import Team from "./Team"

class AddForm extends Component {

    state = {
        goals: '',
        possessionBall: '',
        shots: '',
        freekicks: '',
        corners: '',
        offsides: '',
        team: ''
    }

    handleTeamChange = (team) => {
        this.setState({ team: team });
    }

    handleMatchChange = ({ target }) => {
        let { name, value } = target;
        let team = this.state;
        team[name] = value;
        this.setState({ [name]: value });
        this.props.onMatchChange(team);
    }


    componentDidMount() {
        if (this.props.team !== undefined) {
            this.setState(this.props.team);
        }
    }


    render() {

        let team = null;
        if (this.props.team.team !== undefined) {
            team = <h5>{this.props.team.team}</h5>
        }
        else
        {
            team=<Team onTeamChange={this.handleTeamChange} teams={this.props.teams}></Team>
        }
        return (
            <div>
                {team}
                <div className="form-group row">
                    <label>Broj golova:</label>
                    <div className="col-10">
                        <input className="form-control" value={this.state.goals} type="text" name="goals" onChange={this.handleMatchChange} />
                    </div>
                </div>

                <div className="form-group row">
                    <label>Posed lopte:</label>
                    <div className="col-10">
                        <input className="form-control" value={this.state.possessionBall} type="text" name="possessionBall" onChange={this.handleMatchChange} />
                    </div>
                </div>

                <div className="form-group row">
                    <label>Broj suteva:</label>
                    <div className="col-10">
                        <input className="form-control" type="text" value={this.state.shots} name="shots" onChange={this.handleMatchChange} />
                    </div>
                </div>

                <div className="form-group row">
                    <label>Broj slobodnih udaraca:</label>
                    <div className="col-10">
                        <input className="form-control" type="text" value={this.state.freekicks} name="freekicks" onChange={this.handleMatchChange} />
                    </div>
                </div>

                <div className="form-group row">
                    <label>Broj Kornera:</label>
                    <div className="col-10">
                        <input className="form-control" type="text" value={this.state.corners} name="corners" onChange={this.handleMatchChange} />
                    </div>
                </div>

                <div className="form-group row">
                    <label>Broj Ofsajda:</label>
                    <div className="col-10">
                        <input className="form-control" type="text" value={this.state.offsides} name="offsides" onChange={this.handleMatchChange} />
                    </div>
                </div>

            </div>

        );
    }
}
export default AddForm;