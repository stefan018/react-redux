import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';


import Home from "./Home";
import Matches from "./Matches";
import Error from "./Error";
import EditForm from "./EditForm";
import Navigation from "./Navigation";
import Results from "./Results";


import { requestFetchMatches, deleteMatchAction, searchMatchAction } from '../actions/match-action';
import { requestFetchTeams } from '../actions/teams-action';


class Main extends Component {

    componentDidMount() {
        this.props.requestFetchMatches();
        this.props.requestFetchTeams();
    }

    handleRoundChange = (round) => {
        this.props.onSearchMatchAction(round);
    }

    render() {

        return (
            
            <div className="App">
                <div>

                    <BrowserRouter>
                        <Navigation />
                        <Switch>
                            <Route path="/utakmice/:id" render={(props) => <EditForm {...props} data={this.props.data} teams={this.props.teams} />} exact></Route>
                            <Route path="/" render={(props) => <Home {...props} teams={this.props.teams} />} exact></Route>
                            <Route path="/utakmice" render={(props) => <Matches {...props} data={this.props.matches} onRoundChange={this.handleRoundChange} />} exact></Route>
                            <Route path="/rezultati" render={(props) => <Results {...props} data={this.props.data} />} exact></Route>
                            <Route component={Error}></Route>
                        </Switch>
                    </BrowserRouter>

                </div>
            </div>

        );
    }

}

const MapStateToProps = (state) => {
    const { round } = state.searchMatchReducer;
    const data=state.matchReducer;
    if (round !== '') {
        return {
            data: state.matchReducer,
            matches: data.filter(match=> match.round.toString().includes(round)===true),
            teams: state.teamsReducer,
        }
    }
    else {
        return {
            matches: state.matchReducer,
            data: state.matchReducer,
            teams: state.teamsReducer,
        }
    }
}


const MapDispatchToProps = (dispatch) => {
    return {
        requestFetchMatches: () => {
            dispatch(requestFetchMatches());
        },
        requestFetchTeams: () => {
            dispatch(requestFetchTeams());
        },
        onDeleteMatchAction: (matchId) => {
            dispatch(deleteMatchAction(matchId));
        },
        onSearchMatchAction: (round) => {
            dispatch(searchMatchAction(round));
        }

    }
}

export default connect(MapStateToProps, MapDispatchToProps)(Main);

