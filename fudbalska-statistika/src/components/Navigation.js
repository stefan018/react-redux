import React from 'react';
import { NavLink } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

const Navigation = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <a className="navbar-brand" href="/">Super Liga</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <NavLink to="/">
                            <li className="nav-item active">
                                <a className="nav-link" >Dodaj utakmicu<span className="sr-only">(current)</span></a>
                            </li>
                        </NavLink>
                        <NavLink to="/utakmice">
                            <li className="nav-item">
                                <a className="nav-link" >Utakmice</a>
                            </li>
                        </NavLink>
                        <NavLink to="/rezultati">
                            <li className="nav-item">
                                <a className="nav-link" >Rezultati</a>
                            </li>
                        </NavLink>
                    </ul>

                </div>
            </nav>
        </div>
    );
}

export default Navigation;