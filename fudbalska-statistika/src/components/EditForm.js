
import React, { Component } from "react";
import Home from "./Home";


class EditForm extends Component {
    render() {
        let list = null;
        if (this.props.data !== undefined) {
            let match = this.props.data.filter(match => {
                if (match.id.toString() === this.props.match.params.id.toString())
                    return match;
            });
            if (match.length > 0) {
                match = match[0];
                match.id = this.props.match.params.id;
                list = <Home match={match} teams={this.props.teams} />
            }
        }
        return (
            <div>
                {list}
            </div>
        );


    }
}

export default EditForm;

