import React, { Component } from 'react';

class Team extends Component {
    state = {
        team: ''
    }

    handleTeamChange = ({ target }) => {

        let { name, value } = target;
        this.setState({ [name]: value });
        this.props.onTeamChange(value);
    }

    render() {
        if (this.props.team !== undefined) {
            this.setState({ team: this.props.team });
        }
        let list = [];
        if (this.props.teams !== null) {
            if (this.props.teams instanceof Array && this.props.teams.length > 0)
                list = this.props.teams.map(team => <option value={team.name} > {team.name}</option>);
            list.splice(0, 0, <option value=''>Izaberi</option>);
        }


        return (
            <div className="form-group row">

                <div className="col-10">
                    <select value={this.state.team} className="custom-select mb-2 mr-sm-2 mb-sm-0" name="team" onChange={this.handleTeamChange} >
                        {list}
                    </select>
                </div>
            </div>
        );
    }
}

export default Team;