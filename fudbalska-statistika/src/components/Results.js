

import React, { Component } from "react";


class Results extends Component {

    render() {

        let { onDeleteMatch, ...otherProps } = this.props;
        let list = null;
        if (this.props.data !== null) {
            const data = this.props.data.sort((a, b) => {
                if (parseInt(a.round) > parseInt(b.round))
                    return 1;
                else
                    return -1;
            });
            if (data instanceof Array && data.length > 0)
                list = data.map(match =>
                    <tr>
                        <td> {match.round}</td>
                        <td>{match.Hteam}</td>
                        <td>{match.Ateam}</td>
                        <td>{match.Hgoals}</td>
                        <td>{match.Agoals}</td>
                    </tr>
                );

            return (

                <div className="container">

                    <h3 className="text-center">Rezultati</h3>
                    <table class="table">
                        <tr>
                            <th>Br. kola</th>
                            <th>Domacin</th>
                            <th>Gost</th>
                            <th>Br. golova Domacin</th>
                            <th>Br. golova Gost </th>
                        </tr>
                        {list}
                    </table>
                </div>
            );
        }
    }
}

export default Results;