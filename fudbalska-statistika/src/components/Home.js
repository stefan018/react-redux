import { connect } from 'react-redux';
import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import AddForm from './AddForm';
import { addMatchAction, updateMatchAction } from '../actions/match-action';


class Home extends Component {
    state = {
        round: 1,
        Hgoals: -1,
        HpossessionBall: -1,
        Hshots: -1,
        Hfreekicks: -1,
        Hcorners: -1,
        Hoffsides: -1,
        Hteam: -1,
        Agoals: -1,
        ApossessionBall: -1,
        Ashots: -1,
        Afreekicks: -1,
        Acorners: -1,
        Aoffsides: -1,
        Ateam: -1,
        formErrors: {
            goals: 'Popunite ispravno polje za golove',
            possessionBall: 'Popunite ispravno polje za posed',
            shots: 'Popunite ispravno polje za suteve',
            freekicks: 'Popunite ispravno polje za slobodne udarce',
            corners: 'Popunite ispravno polje za kornere',
            offsides: 'Popunite ispravno polje za ofsajde',
            team: 'Izaberite ispravno tim iz padajuceg menija',
        },
        errorMessages: ''
    }

    handleMatchChange = ({ target }) => {
        let { name, value } = target;
        this.setState({ [name]: value });
    }

    handleHomeMatch = (match) => {
        this.setState(
            {
                Hgoals: match.goals,
                HpossessionBall: match.possessionBall,
                Hshots: match.shots,
                Hfreekicks: match.freekicks,
                Hcorners: match.corners,
                Hoffsides: match.offsides,
                Hteam: match.team
            });
        this.validation(match);
    }

    handleAwayMatch = (match) => {
        this.setState(
            {
                Agoals: match.goals,
                ApossessionBall: match.possessionBall,
                Ashots: match.shots,
                Afreekicks: match.freekicks,
                Acorners: match.corners,
                Aoffsides: match.offsides,
                Ateam: match.team
            });
        this.validation(match);
    }

    validation = (match) => {
        let { formErrors } = this.state;
        if (match.goals !== -1 && match.goals !== '' && parseInt(match.goals) > -1 && parseInt(match.goals) < 100) {
            formErrors.goals = '';
        }
        else {
            formErrors.goals = "Popunite ispravno polje za golove";
        }
        if (match.team !== 'Izaberi' && match.team !== '') {
            formErrors.team = '';
        }
        else {
            formErrors.team = "Izaberite ispravno tim iz padajuceg menija";
        }
        if (match.possessionBall !== -1 && match.possessionBall !== '' && parseInt(match.possessionBall) > -1 && parseInt(match.possessionBall) < 100) {
            formErrors.possessionBall = '';
        }
        else {
            formErrors.possessionBall = "Popunite ispravno polje za posed!";
        }
        if (match.shots !== -1 && match.shots !== '' && parseInt(match.shots) > -1 && parseInt(match.shots) < 100) {
            formErrors.shots = '';
        }
        else {
            formErrors.shots = "Popunite ispravno polje za suteve";
        }
        if (match.freekicks !== -1 && match.freekicks !== '' && parseInt(match.freekicks) > -1 && parseInt(match.freekicks) < 100) {
            formErrors.freekicks = '';
        }
        else {
            formErrors.freekicks = "Popunite ispravno polje za slobodne udarce";
        }
        if (match.corners !== -1 && match.corners !== '' && parseInt(match.corners) > -1 && parseInt(match.corners) < 100) {
            formErrors.corners = '';
        }
        else {
            formErrors.corners = "Popunite ispravno polje za kornere";
        }
        if (match.offsides !== -1 && match.offsides !== '' && parseInt(match.offsides) > -1 && parseInt(match.offsides) < 100) {
            formErrors.offsides = '';
        }
        else {
            formErrors.offsides = "Popunite ispravno polje za ofsajde";
        }
        this.setState({ formErrors });

    }

    submitHandler = (evt) => {
        evt.preventDefault();

        let { formErrors } = this.state;
        let errorMessages = Object.values(formErrors).map(
            (err, index) => err.length === 0 ? null : <li key={index}>{err}</li>);

        if (errorMessages.length > -1) {
            errorMessages = errorMessages.filter(err => err !== null);
        }

        if (this.props.match.id !== undefined && errorMessages.length === 7) {

            errorMessages = <li key={1}>Niste izvrsili nikakvu promenu</li>;
        }
        this.setState({ errorMessages })
        if (this.validateForm(this.state.formErrors)) {
            let newMatch = this.state;
            if (this.props.match.id === undefined) {

                this.props.onAddMatch(newMatch);
            }
            else {
                newMatch.id = this.props.match.id;
                this.props.onUpdateMatch(newMatch);
            }
        }


    }

    validateForm = formErrors => {
        let valid = true;
        Object.values(formErrors).forEach(val => valid = valid && val.length === 0);
        return valid;
    }

    componentDidMount() {
        if (this.props.match !== undefined) {
            this.setState(this.props.match);
        }
    }

    render() {

        let Hometeam = null;
        let Awayteam = null;
        if (this.props.match !== undefined) {
            Hometeam = {
                goals: this.props.match.Hgoals,
                possessionBall: this.props.match.HpossessionBall,
                shots: this.props.match.Hshots,
                freekicks: this.props.match.Hfreekicks,
                corners: this.props.match.Hcorners,
                offsides: this.props.match.Hoffsides,
                team: this.props.match.Hteam
            }
            Awayteam = {
                goals: this.props.match.Agoals,
                possessionBall: this.props.match.ApossessionBall,
                shots: this.props.match.Ashots,
                freekicks: this.props.match.Afreekicks,
                corners: this.props.match.Acorners,
                offsides: this.props.match.Aoffsides,
                team: this.props.match.Ateam
            }
        }
        let back = null;
        if (this.props.match.id !== undefined) {
            back = <NavLink to="/utakmice"><a>Odustani</a></NavLink>
        }
        return (

            <div className="d-flex justify-content-center">
                <div className="form-group row">
                    <div className="col-10">
                        <h3 className="text-center">Super liga Srbije</h3>
                    </div>
                    <form >
                        <br></br>
                        <div className="form-group row">
                            <label htmlFor="example-number-input" className="col-2 col-form-label">Kolo: </label>
                            <div className="col-6">
                                <input className="form-control" type="text" value={this.state.round} name="round" placeholder="Broj kola:" onChange={this.handleMatchChange} />
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="row">
                                <div className="col">
                                    <div className="form-group row">
                                        <h6>Domacin</h6>
                                    </div>

                                    <AddForm onMatchChange={this.handleHomeMatch} teams={this.props.teams} team={Hometeam} />
                                </div>
                                <div className="col">
                                    <div className="form-group row">
                                        <h6>Gost</h6>
                                    </div>
                                    <AddForm onMatchChange={this.handleAwayMatch} teams={this.props.teams} team={Awayteam} />
                                </div>
                            </div>
                            <div className="form-group row">
                                <div className="col-10">
                                    <button className="btn btn-primary" onClick={this.submitHandler}>Sacuvaj</button>
                                </div>
                                {back}
                            </div>
                        </div>
                        <ul>
                            {this.state.errorMessages}
                        </ul>
                    </form>

                </div>
            </div>

        );
    }
};

const MapDispatchToProps = (dispatch) => {
    return {
        onAddMatch: (newMatch) => {
            dispatch(addMatchAction(newMatch));
        },
        onUpdateMatch: (updatedMatch) => {
            dispatch(updateMatchAction(updatedMatch));
        }
    }
}

export default connect(null, MapDispatchToProps)(Home);